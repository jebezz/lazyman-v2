
package GameObj;


public class GameStream {

    private String aMID, hMID, nMID, fMID, threeCamMID, sixCamMID, ma1MID, ma2MID, ma3MID, aTV, hTV, nTV, fTV;

    
    public void setAwayMediaID(String id) {
        aMID=id;
    }

    
    public String getAwayMediaID() {
        return aMID;
    }

    
    public void setHomeMediaID(String id) {
        hMID=id;
    }

    
    public String getHomeMediaID() {
        return hMID;
    }

    
    public void setNationalMediaID(String id) {
        nMID = id;
    }

    
    public String getNationalMediaID() {
        return nMID;
    }

    
    public void setFrenchMediaID(String id) {
        fMID = id;
    }

    
    public String getFrenchMediaID() {
        return fMID;
    }

    
    public void setAwayTVStation(String station) {
        aTV=station;
    }

    
    public String getAwayTVStation() {
        return aTV;
    }

    
    public void setHomeTVStation(String station) {
        hTV=station;
    }

    
    public String getHomeTVStation() {
        return hTV;
    }

    
    public void setNationalTVStation(String station) {
        nTV=station;
    }

    
    public String getNationalTVStation() {
        return nTV;
    }

    
    public void setFrenchTVStation(String station) {
        fTV=station;
    }

    
    public String getFrenchTVStation() {
        return fTV;
    }


    public String getThreeCamMID() {
        return threeCamMID;
    }

    public void setThreeCamMID(String threeCamMID) {
        this.threeCamMID = threeCamMID;
    }

    public String getSixCamMID() {
        return sixCamMID;
    }

    public void setSixCamMID(String sixCamMID) {
        this.sixCamMID = sixCamMID;
    }

    public String getMa1MID() {
        return ma1MID;
    }

    public void setMa1MID(String ma1MID) {
        this.ma1MID = ma1MID;
    }

    public String getMa2MID() {
        return ma2MID;
    }

    public void setMa2MID(String ma2MID) {
        this.ma2MID = ma2MID;
    }

    public String getMa3MID() {
        return ma3MID;
    }

    public void setMa3MID(String ma3MID) {
        this.ma3MID = ma3MID;
    }
    
   
    
}
